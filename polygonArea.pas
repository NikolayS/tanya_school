const
  nmax = 100;
var
  n, i: integer;
  isConvex: boolean;
  x: array[1..nmax] of integer;
  y: array[1..nmax] of integer;
  triangleArea, prevTriangleArea, polygonArea: real;

function pseudoscalar(x1, y1, x2, y2, x3, y3: integer): real;
begin
   pseudoscalar := ((x2 - x1) * (y3 - y2) - (y2 - y1) * (x3 - x2)) / 2;
end;

begin
  // get values from user
  read(n);
  for i := 1 to n do
    read(x[i], y[i]);

  isConvex := true;
  polygonArea := 0;
  prevTriangleArea := 1;

  // One loop for both goals: determine if the polygon is convex + get its area
  for i := 1 to n do begin
    triangleArea := pseudoscalar(
      x[i], y[i],
      x[i mod n + 1], y[i mod n + 1],
      x[(i + 1) mod n + 1], y[(i + 1) mod n + 1]
    );

    //writeln('DEBUG: triangleArea ', triangleArea, '; prevTriangleArea', prevTriangleArea);
    if (isConvex) and (i > 1) and (prevTriangleArea * triangleArea < 0) then
      isConvex := false;

    // Remember the previous triangle area to compare signs in the next loop
    prevTriangleArea := triangleArea;

    // Area calculation
    polygonArea := polygonArea
      + (
        (x[i] * y[i mod n + 1]) - (y[i] * x[i mod n + 1])
      ) / 2;
  end;

  write('Выпуклость: ', isConvex, ', площадь: ', abs(polygonArea):10:2);
end.
