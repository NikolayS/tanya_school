// test comment
var
  n, m, k, d, sd, i: integer;
begin
  read(n, m);
  k := 0;
  d := 1;
  sd := 0;

  for i := n to m do begin
    repeat
      if i mod d = 0 then sd := sd + d;
      d := d + 1;
    until sd >= i;

    if d >= i div 2 then
      if sd = i then begin
        k := k + 1;
        writeln('  ', i);
      end;

    sd := 0;
    d := 1;
  end;

  write(k);
end.