﻿var
  x1, y1, x2, y2, r1, r2, a: integer;
  r: real;
begin
  read(x1, y1, r1, x2, y2, r2);
  if r1 < r2 then
    begin
      a := x1;
      x1 := x2;
      x2 := a;
      a := y1;
      y1 := y2;
      y2 := a;
      a := r1;
      r1 := r2;
      r2 := a;
    end;
  r := sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
  if (r1 + r2 < r) and (r1 < r + r2) then write('no')
  else write('yes');
end.