﻿var
  i, a1, a2, b1, b2, i1, g: integer;
  a: array[1..15] of integer;
begin
  a1 := 0;
  a2 := 0;
  b1 := 0;
  b2 := 0;
  randomize;
  for i := 1 to 15 do
    begin
      a[i] := random(61) - 30;
      write(a[i] : 4);
    end;
  for i := 1 to 15 do
    begin
      g := 0;
      i1 := 1;
      repeat
        if a[i1] > 0 then a1 := i1;
        i1 := i1 + 1;
      until (a1 > 0) or (i1 = 16);
      i1 := a1 + 1;
      a2 := a1;
      repeat
        if a[i1] > 0 then a2 := a2 + 1
        else g := 1;
        i1 := i1 + 1;
      until (a1 > 0) or (g = 1);
      if a2 - a1 > b2 - b1 then
        begin
          b1 := a1;
          b2 := a2;
        end;
    end;
  writeln(' ');
  for i := b1 to b2 do
    write(a[i] : 4);
end.