﻿var
  mass: array[1..10] of integer;
  a1, ak, i, b: integer;
begin
  for i := 1 to 10 do
    begin
      read(mass[i]);
      if (mass[i] > 0) and (mass[i] mod 2 = 0) then ak := mass[i];
    end;
  i := 1;
  b := 0;
  repeat
    if (mass[i] > 0) and (mass[i] mod 2 = 0) then
      begin
        a1 := mass[i];
        b := 1;
      end;
    i := i + 1;
  until (i = 11) or (b = 1);
  write(' ', a1, ' ', ak);
end.