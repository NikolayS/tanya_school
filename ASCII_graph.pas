﻿var
  y: array[1..100] of integer;
  n, m, maxes, i, yCounter, graph, b: integer;
begin
  read(n, m);
  for i := 1 to n do
    begin
      read(graph);
      yCounter := m;
      repeat
        if graph mod 10 = 2 then
          begin
            b := 1;
            y[m - yCounter + 1] := i;
          end;
        yCounter := yCounter - 1;
        graph := graph div 10;
      until (b = 1) or (yCounter = -1);
    end;
  for i := 1 to m do writeln(y[i]:4);
  for i := 2 to (m - 1) do
    if (y[i] > y[i - 1]) and (y[i] > y[i + 1]) then maxes := maxes + 1;
  if y[1] > y[2] then maxes := maxes + 1;
  if y[m] > y[m - 1] then maxes := maxes + 1;
  write(maxes);
end.