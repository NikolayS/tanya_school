﻿var
  xa, ya, xb, yb, xc, yc, xd, yd: integer;
  s1, s2, s3, s, a, b, c, da, db, dc, p, p1, p2, p3: real;
begin
  readln(xa, ya);
  readln(xb, yb);
  readln(xc, yc);
  readln(xd, yd);
  a := sqrt((xb - xc) * (xb - xc) + (yb - yc) * (yb - yc));
  b := sqrt((xa - xc) * (xa - xc) + (ya - yc) * (ya - yc));
  c := sqrt((xb - xa) * (xb - xa) + (yb - ya) * (yb - ya));
  da := sqrt((xa - xd) * (xa - xd) + (ya - yd) * (ya - yd));
  db := sqrt((xb - xd) * (xb - xd) + (yb - yd) * (yb - yd));
  dc := sqrt((xc - xd) * (xc - xd) + (yc - yd) * (yc - yd));
  p := 0.5 * (a + b + c);
  p1 := 0.5 * (c + da + dc);
  p2 := 0.5 * (b + da + dc);
  p3 := 0.5 * (a + db + dc);
  s := sqrt(p * (p - a) * (p - b) * (p - c));
  s1 := sqrt(p1 * (p - c) * (p1 - da) * (p1 - dc));
  s2 := sqrt(p2 * (p2 - b) * (p2 - da) * (p2 - dc));
  s3 := sqrt(p3 * (p3 - a) * (p3 - db) * (p3 - dc));
  //write(s1 + s2 + s3 - s);
  if s1 + s2 + s3 - s < 1 then write('yes')
  else write('no');
end.