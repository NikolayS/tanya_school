﻿var
  a: array[1..10] of integer;
  ap, b, i: integer;
begin
  read(a[1]);
  ap := a[1];
  b := 0;
  i := 1;
  repeat
    i := i + 1;
    read(a[i]);
    if a[i] < ap then b := 1;
    ap := a[i];
  until (i = 10) or (b = 1);
  if b = 0 then write('yep')
  else write('nope');
end.