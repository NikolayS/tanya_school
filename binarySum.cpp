#include <iostream>

using namespace std;

int main() {
    int i, j, a, b, aa[8], bb[8], bn, s[8];
    cin >> a >> b;
    aa[0] = 0;
    
    for (i = 0; i < 8; i++) {
        s[i] = 0;
    }
    
    bn = abs(b);
    
    for (i = 7; i > 1; i--) {
        aa[i] = a % 2;
        a = a / 2;
        bb[i] = bn % 2;
        bn = bn / 2;
    }
    
    cout << "a ";
    for (i = 0; i < 8; i++) {
        cout << aa[i];
    }
    cout << endl;
    
    if (b >= 0) {
        bb[0] = 0;
        cout << "b ";
        for (j = 0; j < 8; j++) {
            cout << bb[j];
        }
        cout << endl;
        for (j = 8; j > 0; j--) {
            s[j] = s[j] + aa[j] + bb[j];
            if (s[j] == 2) {
                s[j] = 0;
                s[j - 1] = 1;
            }
            if (s[j] == 3) {
                s[j] = 1;
                s[j - 1] = 1;
            }
        }
        for (j = 0; j < 8; j++) {
            cout << s[j];
        }
    } else {
        bb[0] = 1;
        cout << "Прямой: " << "b ";
        for (j = 0; j < 8; j++) {
            cout << bb[j];
        }
        cout << endl;
        for (j = 8; j > 0; j--) {
            s[j] = s[j] + aa[j] + bb[j];
            if (s[j] == 2) {
                s[j] = 0;
                s[j - 1] = 1;
            }
            if (s[j] == 3) {
                s[j] = 1;
                s[j - 1] = 1;
            }
        }
        for (j = 0; j < 8; j++) {
            cout << s[j];
        }
        
        for (j = 0; j < 8; j++) {
            s[j] = 0;
        }
        cout << endl;
        
        cout << "Обратный: " << "b ";
        for (j = 1; j < 8; j++) {
            bb[j] = (bb[j] + 1) % 2;
        }
        for (j = 0; j < 8; j++) {
            cout << bb[j];
        }
        cout << endl;
        for (j = 8; j > 0; j--) {
            s[j] = s[j] + aa[j] + bb[j];
            if (s[j] == 2) {
                s[j] = 0;
                s[j - 1] = 1;
            }
            if (s[j] == 3) {
                s[j] = 1;
                s[j - 1] = 1;
            }
        }
        for (j = 0; j < 8; j++) {
            cout << s[j];
        }
        cout << endl;
        
        cout << "Прямой: " << "b ";
        bb[8]++;
        for (j = 8; j > 1; j--) {
            if (bb[j] == 2) {
                bb[j] = 0;
                bb[j - 1] = 1;
            }
            if (bb[j] == 3) {
                bb[j] = 1;
                bb[j - 1] = 1;
            }
        }
        for (j = 0; j < 8; j++) {
            cout << bb[j];
        }
        cout << endl;
        for (j = 8; j > 0; j--) {
            s[j] = s[j] + aa[j] + bb[j];
            if (s[j] == 2) {
                s[j] = 0;
                s[j - 1] = 1;
            }
            if (s[j] == 3) {
                s[j] = 1;
                s[j - 1] = 1;
            }
        }
        for (j = 0; j < 8; j++) {
            cout << s[j];
        }
    }
    
    return 0;
}