﻿var
  mass: array[1..20] of integer;
  a1, ak, i, b: integer;
  ar: real;
begin
  randomize;
  b := 0;
  a1 := -1;
  ak := -1;
  for i := 1 to 20 do
    begin
      mass[i] := random(51);
      write(mass[i] : 4);
      if (mass[i] mod 5 = 0) and (b = 0) then
        begin
          a1 := mass[i];
          b := 1;
        end
      else if mass[i] mod 5 = 0 then ak := mass[i];
    end;
  writeln(' ');
  if ak = -1 then ak := a1;
  if a1 <> -1 then
    begin
      ar := (a1 + ak) / 2;
      write(ar);
    end
  else write('nope');
end.