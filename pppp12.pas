const
  n = 18;

var
  x, y, a1, a2, i: integer;
  fib: array[1..n] of integer;

begin
  fib[1] := 1;
  fib[2] := 1;
  for i := 3 to 18 do
    fib[i] := fib[i - 1] + fib[i - 2];
  read(x, y);
  x := x - 2;
  a1 := 0;
  a2 := 0;
  repeat
    a1 := a1 + 1;
    if (y - fib[x] * a1) mod fib[x + 1] = 0 then
      a2 := (y - fib[x] * a1) div fib[x + 1];
  until a2 <> 0;
  write(a1, a2 : 3);
end.