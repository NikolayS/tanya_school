var
  n, animalsInTotal, i, a, j, b1: integer;
  SUMexcessCombinations, combinationsInTotal: real;
  animalsInKind: array[1..10] of integer;
  excessCombinations: array[1..20] of real;

function factorial(n, k, b: integer): integer;
  begin
    factorial := 1;
    for k := 1 to n do
      b := b * k;
    factorial := b;
  end;

begin
  readln(n);
  
  //read
  for i := 1 to n do
    begin
        read(animalsInKind[i]);
    end;
  
  //count animals in total
  for i := 1 to n do
    animalsInTotal := animalsInTotal + animalsInKind[i];
  
  //count excess combinations
  for i := 1 to n do
    begin
      if animalsInKind[i] > 1 then
        begin
          if animalsInKind[i] = 2 then
            excessCombinations[i] := animalsInTotal - 2
          else
            begin
              excessCombinations[i] := excessCombinations[i] + (factorial(animalsInKind[i], j, b1) / (2 * factorial(animalsInKind[i] - 2, j, b1)));
              if animalsInKind[i] = 3 then
                excessCombinations[i] := excessCombinations[i] + 1
              else
                excessCombinations[i] := excessCombinations[i] + (factorial(animalsInKind[i], j, b1) / (3 * factorial(animalsInKind[i] - 3, j, b1)));
            end;
        end;
    end;
  for i := 1 to n do
    SUMexcessCombinations := SUMexcessCombinations + excessCombinations[i];
  
  combinationsInTotal := (factorial(animalsInTotal, j, b1) / (3 * factorial(animalsInTotal - 3, j, b1))) - SUMexcessCombinations;
  write(combinationsInTotal);
end.