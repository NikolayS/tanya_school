var
  a, n, f1, f2, f3, ak, b: integer;
begin
  read(a);
  f1 := 1;
  f2 := 2;
  f3 := f1 + f2;

  n := 2;
  repeat
    f1 := f2;
    f2 := f3;
    f3 := f1 + f2;
    n := n + 1;
  until a < f3;

  ak := 1;
  a := a - f2;

  while n >= 2 do begin
    if a < f1 then
      ak := ak * 10
    else begin
      b := a div f1;
      ak := ak * 10 + b;
      a := a - f1;
    end;

    f3 := f2;
    f2 := f1;
    f1 := f3 - f2;

    n := n - 1;
  end;

  write(ak);
end.