﻿var
  n, k: integer;
begin
  read(n);
  k := (n*(n + 1) / 2) * (n + 2);
  write(k);
end.