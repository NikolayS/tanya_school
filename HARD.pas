﻿var
  input: array[1..12] of integer;
  output: array[1..12] of integer;
  n, k, nf, i, length, box, kVar, nVar: integer;
begin
  read(n, k);

  // initialization
  for i := 1 to n do
    input[i] := i;
  kVar := k;
  nVar := n;
  
  repeat
    // calculate factorial for nVar
    nf := 1;
    for i := 2 to nVar do
      nf := nf * i;

    // get: length of boxes, current box, next n and next target k
    length := nf div nVar;
    box := (kVar - 1)  div length + 1; // find the box, which k belongs to
    kVar := kVar  mod length; // new k; this will be used in the next cycle
    if kVar = 0 then kVar := length;
    nVar := nVar - 1;
 
    // what goes to the output
    output[n - nVar] := input[box];
    
    // redefine input
    for i := box to n - 1 do
      input[i] := input[i + 1];
  until nVar = 0;

  // write result
  for i := 1 to n do
    write(output[i] : 4);
end.